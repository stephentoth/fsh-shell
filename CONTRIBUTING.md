# Contributing
Before you fork and change anything, please read this file!

## Conventions
This project *may* contain some conventions not found in other simalar projects. The main code styles and locations of use are listed below.

### Universal
* Indents
Indents are inportant to keep code readable. The superior indent character is space, so. You should indent when:
   * Pushing to stack (unindent when poping)
   * ex instruction (only when the ex is temporary and reversed)
   * Loops
* Routines
  * Routines should have the following header:
    ```asm
    ;;name
    ;;Discription
    ;;Inputs:
    ;; HL: purpose
    ;;Outputs:
    ;; HL: ...
    ```
    * Inputs/Outputs only if needed.
  * Labels
    * Name of routine must be lowercase
    * Name of routine must have prefix of file name
    * If there are lables in a routine:
      * label must be name of routine + Suffix (capitalized):
      `fileroutineSuffix`
  * Keep to `jr` in routines (unless impossible)
  * All routines MUST ret at the end (no jp)
  * Routines MUST preserve all registers (except outputs)
  * A routine's instructions should be lowercase

### FSHa.asm
* All instructions/registers/directives are UPPERCASE (strings are fine)
* High level view
  * Only ld, calls, jmps (no routines/macros)
* Keep it readable

### Command Files
* Name of file is UPPERCASE
* Inctructions are lowercase
* The layout of the file must be as follows:
  ```asm
  <nameofmodule>cmd<COMMAND>:  .db "<COMMAND>", 0
  <nameofmodule>cmd<COMMAND>o: .db $<optcode>
  <nameofmodule>cmd<COMMAND>u: .db "<Discription>", 0
  <nameofmodule>cmd<COMMAND>f:
  <Command code to execute>
  jp  Fin
  ```
* Registers CAN be destroyed during execution of command's code
