# Fsh SHell
Fsh SHell is a compact command-line interface for TI-84 Plus CE calculators.

## Installing
* Windows
  1. Download and install a linking program. <https://education.ti.com/ticonnectce> is ti's prepritory software.
  2. Plug in calculator and launch ti connect ce
  3. Drag and drop `bin\FSHa.8xp` to calculator
  4. Done!
* Linux
  1. Install TiLP
  2. Plug in Calculator and launch TiLP
  3. Send over `bin/FSHa.8xp`

## Running
For most people all that is required is to press <kbd>2nd</kbd>+<kbd>0</kbd> and select the Asm( option. Then, press <kbd>Prgm</kbd> and select FSHA. you should now have the following: `Asm(PrgmFSHA` Press <kbd>Enter</kbd>

For TIOS 5.3+
 Press <kbd>Prgm</kbd> and select FSHA. you should now have the following: `PrgmFSHA` Press <kbd>Enter</kbd>

## Building From Source
The source code for this project is available at <https://gitlab.com/stephentoth/fsh-shell>
Download it from the link and then follow the instructions below for different operating systems.

* Linux
  1. Clone, make, and install spasm-ng <https://github.com/alberthdev/spasm-ng>
  2. Run the `build` script

  ```
  ./build
  ```
  
* Windows
  1. Download and build spasm-ng <https://github.com/alberthdev/spasm-ng>
  2. Run `build.bat`

## Contributing
Please see `CONTRIBUTING.md` in the repository.

## Bug Reports
If you find an Issue or Bug, please let me know by making an Issue on Gitlab: <https://gitlab.com/stephentoth/fsh-shell>
Please include:
1. What went wrong.
2. What you were doing when the issue occurred.
3. Possible cause/fixes? (optional)

Thank you!
