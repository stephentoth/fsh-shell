;;rgbto565
;;turns an rgb input into the 656 format that the lcd uses
;;Inputs:
;; r,g,b
;;Outputs:
;; HL: color
#macro rgbto565(red, green, blue)
 ld hl, ((red / 255 * 31) << 11) | ((green / 255 * 63) << 5) | ((blue / 255 * 31))
#endmacro
