;;pixel
;;Gets vRam location of coordinates
;;Inputs:
;; x: x axis
;; y: y axis
;;Outputs:
;; DE: vRam location
#macro pixel(x, y)
 ld de, vRam + (y * 320 * 2) + (x * 2)
#endmacro
