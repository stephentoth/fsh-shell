#macro drawpixelxyM(x, y)
 ld de, x
 ld a, y
 call drawpixelxy
#endmacro

#macro drawlineM(x1, y1, x2, y2)
;TODO
#endmacro

#macro drawrowM(y)
 ld a, y
 call drawrow
#endmacro

#macro drawbarM(y, height)
 ld a, y
 ld bc, height
 call drawbar
#endmacro

#macro drawrectfilledM(x, y, width, height) 
 ex de, hl
  ld ixl, e
  ld ixh, d
 ex de, hl
 ld hl, width
 ld de, x
 ld bc, 0
 ld c, y
 ld a, height
 call drawrectfilled
#endmacro
