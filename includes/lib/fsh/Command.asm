;;commandfill
;;Fills command mem with byte a
;;Inputs:
;; A: byte
commandfill:
 push hl
  push bc
   push de
    ld b, 0
    ld de, 0
    ld c, a
    ld hl, FshCmd
commandfillLoop:
    ld (hl), c
    inc hl
    inc b
    inc de
    ld a, b
    cp FshCmdSize
    jr c, commandfillLoop
   pop de
  pop bc
 pop hl
 ret
