;;fshparsecommand
;;Inputs:
;; HL: FshCmd
;;b = buf row
;;c = char in row
;de = scratch
fshparsecommand:
 push hl
 push bc
 push de
  push hl
   push hl
    ld hl, fshparsecommandCmd
    ld b, 45
    ld a, $00
    call memset
   pop hl

   ld bc, 0
   ld de, 0
fshparsecommandLoop:
   ld a, (hl)
fshparsecommandCheckSpace: 
   cp ' ' ;end of word
   jr nz, fshparsecommandCheckEnd
   ld a, $00
   call fshparsecommandCopyChar
   ld c, 0
   inc b
   inc hl
   jr fshparsecommandLoop

fshparsecommandCheckEnd:
   cp $00 ;end of string
   jr z, fshparsecommandCheckArgStatus

fshparsecommandLoopEnd:
   call fshparsecommandCopyChar
   inc hl
   inc c

   ld a, c
   cp FshCmdCmdSize
   jr nz, fshparsecommandLoop
   ;inc hl
   ld c, 0
   inc b
   ld a, b
   cp 5
   jr nz, fshparsecommandLoop
;   ld bc, 0

fshparsecommandCheckArgStatus:
   xor a
   ld hl, FshCmdArgFlags
   ld (hl), a
fshparsecommandCheckArg0:
   ld hl, fshparsecommandArg0
   ld a, (hl)
   cp $00
   jr z, fshparsecommandCheckArg1
   ld hl, FshCmdArgFlags
   set 0, (hl) 
   ld a, (hl)
   ld hl, fshparsecommandArg0
   ld a, (hl)
   sub '0' 
   cp '9' - '0' + 1
   jr nc, fshparsecommandCheckArg1
   call strtow
   ld de, fshparsecommandArg0
   ex de, hl
   ld (hl), e
   inc hl
   ld (hl), d
   inc (hl)
   ld (hl), $00
   ld hl, FshCmdArgFlags
   set 1, (hl)
fshparsecommandCheckArg1:
   ld hl, fshparsecommandArg1
   ld a, (hl)
   cp $00
   jr z, fshparsecommandCheckArg2
   ld hl, FshCmdArgFlags
   set 2, (hl) 
   ld hl, fshparsecommandArg1
   ld a, (hl)
   sub '0'
   cp '9' - '0' + 1
   jr nc, fshparsecommandCheckArg2
   call strtow
   ld de, fshparsecommandArg1
   ex de, hl
   ld (hl), e
   inc hl
   ld (hl), d
   inc hl
   ld (hl), $00
   ld hl, FshCmdArgFlags
   set 3, (hl)
fshparsecommandCheckArg2:
   ld hl, fshparsecommandArg2
   ld a, (hl)
   cp $00
   jr z, fshparsecommandCheckArg3
   ld hl, FshCmdArgFlags
   set 4, (hl) 
   ld hl, fshparsecommandArg2
   ld a, (hl)
   sub '0'
   cp '9' - '0' + 1
   jr nc, fshparsecommandCheckArg3
   call strtow
   ld de, fshparsecommandArg2
   ex de, hl
   ld (hl), e
   inc hl
   ld (hl), d
   inc hl
   ld (hl), $00
   ld hl, FshCmdArgFlags
   set 5, (hl)
fshparsecommandCheckArg3:
   ld hl, fshparsecommandArg3
   ld a, (hl)
   cp $00
   jr z, fshparsecommandCopyBack
   ld hl, FshCmdArgFlags
   set 6, (hl) 
   ld hl, fshparsecommandArg3
   ld a, (hl)
   sub '0'
   cp '9' - '0' + 1
   jr nc, fshparsecommandCopyBack
   call strtow
   ld de, fshparsecommandArg3
   ex de, hl
   ld (hl), e
   inc hl
   ld (hl), d
   inc hl
   ld (hl), $00
   ld hl, FshCmdArgFlags
   set 7, (hl)
fshparsecommandCopyBack:
  pop hl
  push bc
   ex de, hl
   ld hl, fshparsecommandCmd
   ld bc, 45
   ldir
  pop bc

 pop de
 pop bc
 pop hl
 ret

fshparsecommandCmd:   .db $00, $00, $00, $00, $00, $00, $00, $00, $00
fshparsecommandArg0:  .db $00, $00, $00, $00, $00, $00, $00, $00, $00
fshparsecommandArg1:  .db $00, $00, $00, $00, $00, $00, $00, $00, $00
fshparsecommandArg2:  .db $00, $00, $00, $00, $00, $00, $00, $00, $00
fshparsecommandArg3:  .db $00, $00, $00, $00, $00, $00, $00, $00, $00
fshparsecommandOverB: .db $00, $00, $00, $00, $00, $00

fshparsecommandCopyChar:
 ;Copy 
 push hl
  push bc
   ld hl, 0
   ld h, b
   ld l, 9
   ld b, c
   ld de, fshparsecommandCmd
   call array2d
  pop bc
 pop hl
 ;push hl
 ; ld hl, fshparsecommandCmd+45+1
 ; call cpHLDE
 ;pop hl
 ;pop de
 ;ret c
 ;push de
 ld (de), a ;ld buffer 
 ret
