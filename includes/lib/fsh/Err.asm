;;ErrCatch
;;Checks err bit and jp to err route
ErrCatch:
 pop bc
  push hl
  ld hl, FshFlags
  bit 1, (hl)
  pop hl
  jp nz, ErrRoute
 push bc
 ret

;;ErrRoute
;;Routes err codes.
ErrRoute:
 
 ld hl, FshFlags
 res 1, (hl)

 ld hl, FshCmdErrCode
 
 ld de, ErrCmdNotFoundo 
 call bytecmp 
 jp z, ErrCmdNotFoundf

 ld de, ErrOutOfBoundso
 call bytecmp
 jp z, ErrOutOfBoundsf

 ld de, ErrArgso
 call bytecmp
 jp z, ErrArgsf

 ld de, ErrUsageo
 call bytecmp
 jp z, ErrUsagef

 jp ErrDefault


;;ErrSet
;;Sets all things relating to Err codes
;;Inputs:
;; HL: Err_____o
ErrSet:
 push hl
  ld a, (hl)
  ld hl, FshCmdErrCode
  ld (hl), a
  ld hl, FshFlags
  set 1, (hl)
 pop hl
 ret

;;ERR FUNCS-------------------

ErrCmdNotFoundo: .db $01
ErrCmdNotFoundf:
 ld hl, FshCmd
 ld a, (hl)
 cp $00
 jp z, Fin
 ld hl, ErrCmdNotFoundt
 call _PutS
 ld hl, FshCmd
 call _PutS
 call _NewLine
 jp Fin
ErrCmdNotFoundt: .db "error:No Command:", 0

ErrOutOfBoundso: .db $02
ErrOutOfBoundsf:
 ld hl, ErrOutOfBoundst
 call _PutS
 call _NewLine
 jp Fin
ErrOutOfBoundst: .db "error: Out Of Bounds", 0

ErrArgso: .db $03
ErrArgsf:
 ld hl, ErrArgst
 call _PutS
 call _NewLine
 jp Fin
ErrArgst: .db "error: Invalid Arguments", 0

ErrUsageo: .db $04
ErrUsagef:
 ld hl, ErrUsaget
 call _PutS
 ld hl, FshCmd
 call manrouteu
 call _PutS
 call _NewLine
 jp Fin
ErrUsaget: .db "error: Usage: ", 0

ErrDefault:
 ld hl, ErrDefaultt
 call _PutS
 jp Exit
ErrDefaultt: .db "error: Default; Exit", 0
