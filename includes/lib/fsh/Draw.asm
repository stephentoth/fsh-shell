;;drawstatusbar
;;draws the FSH status bar
drawstatusbar:
 ;rgbto565(84, 84, 84)
 ld hl, $52AA
 ld a, 0
 ld bc, 30
 call drawbar
 ;rgbto565(0, 159, 209)
 ld hl, $04FA
 ;ld hl, %1111100000000000
 drawrectfilledM(0, 0, 2, 30)
 call drawbatteryind
 ;draw name
 set fracDrawLFont, (iy + fontFlags)
 ld a, 6
 ld (penRow), a
 ld hl, 7
 ld (penCol), hl
 ld hl, $52AA
 ld (drawBGcolor), hl
 ld hl, $ffff
 ld (drawFGcolor), hl
 ld hl, FshName
 call _VPutS
 ld hl, $ffff
 ld (drawBGcolor), hl
 ld hl, $0000
 ld (drawFGcolor), hl
 ret

drawbatteryind:
 call _DrawBatteryIndicator
 ret


updatestatusbar:
 call drawbatteryind
 ret
