;;printfshnamelong
;;prints Name + Varsv + Version to screen
printfshnamelong:
 push hl
  ld hl, FshName
  call _PutS
  ld a, ' ' 
  call _PutC
  ld hl, FshVVars
  call _PutS
  ld a, ' ' 
  call _PutC
  ld a, 'v' 
  call _PutC
  ld hl, FshVMajor
  call _PutS
  ld hl, FshVSeparator
  call _PutS
  ld hl, FshVMinor
  call _PutS
  ld hl, FshVSeparator
  call _PutS
  ld hl, FshVPatch
  call _PutS
  ld a, ' ' 
  call _PutC
  ld hl, FshVBuild
  call _PutS
 pop hl
 ret 

;;printmotd
;;prints the motd to screen
printmotd:
 call printfshnamelong
 call _NewLine
 call _NewLine
 ld hl, FshMotd2nd
 call _PutS
 call _NewLine
 ld hl, FshMotdHelp
 call _PutS
 call _NewLine
 ret
