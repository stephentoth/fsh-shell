;;fshvarset
;;Sets a Word in FshVars.
;;Inputs:
;; HL: 16b
;; DE: Destination Word Relitive to FshVars
fshvarset:
 push bc 
  ld bc, FshVarsSize / 2 - 1
  call cpBCDE
 pop bc
 jr c, fshvarsetOOBException
 push bc
 push de
 push hl
  ld bc, FshVars
  ex de, hl
   add hl, hl
   add hl, bc 
  ld (hl), e
  inc hl
  ld (hl), d
 pop hl
 pop de
 pop bc
 ret
fshvarsetOOBException:
 push hl
  ld hl, ErrOutOfBoundso
  call ErrSet
 pop hl
 ret

;;fshvarget
;;Gets a Word in FshVars.
;;Inputs:
;; DE: Destination Word Relitive to FshVars
;;Outputs:
;; HL: Word in (DE)
fshvarget:
 push bc 
  ld bc, FshVarsSize / 2 - 1
  call cpBCDE
 pop bc
 jr c, fshvargetOOBException
 push bc
 push de
  ld hl, 0
  ld bc, FshVars
  ex de, hl
   add hl, hl
   add hl, bc 
  ld e, (hl)
  inc hl
  ld d, (hl)
  ex de, hl
 pop de
 pop bc
 ret
fshvargetOOBException:
 push hl
  ld hl, ErrOutOfBoundso
  call ErrSet
 pop hl
 ret
;;fshvarcpy
;;Copies a word.
;;Inputs:
;; HL: Byte pointer
;; DE: Destination
fshvarcpy:
 push bc 
  ld bc, FshVarsSize / 2 - 1
  call cpBCDE
 pop bc
  jr c, fshvarcpyOOBException
 push bc
  ld bc, FshVarsSize / 2
  call cpHLBC
 pop bc
  jr nc, fshvarcpyOOBException
 push de
  push hl
   ld bc, FshVarsSize / 2
   call cpBCDE
   jr c, fshvarcpyOOBException
   ld bc, FshVars
    add hl, hl
    add hl, bc
   ex de, hl
    add hl, hl
    add hl, bc
   ld a, (de)
   ld (hl), a
   inc de
   inc hl
   ld a, (de)
   ld (hl), a
  pop hl
 pop de
 ret

fshvarcpyOOBException:
 push hl
  ld hl, ErrOutOfBoundso
  call ErrSet
 pop hl
 ret

;;fshvarreset
;;Fills fshvars with $00
fshvarreset:
 push hl
  push bc
   push de
    ld b, 0
    ld de, 0
    ld hl, 0
fshvarfillLoop:
    call fshvarset
    inc b
    inc de
    ld a, b
    cp FshVarsSize / 2
    jr c, fshvarfillLoop
   pop de
  pop bc
 pop hl
 ret
