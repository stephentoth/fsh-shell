;;rotletencrypt
;;Encrypts a string in rot-n (uses $41 to $5B)
;;Inputs:
;; HL: string pointer
;; A: rot-n
;;Notes:
;; Changes string in place.
rotletencrypt:
 push hl
  push bc
rotletencryptMod:
  push hl
   ld h, a
   ld l, 'Z' + 1 - 'A' - 1
   call modHL
   ld b, h
  pop hl
rotletencryptLoop:
   ld a, (hl)
   cp a, $00
   jr z, rotletencryptEnd
   cp a, ' '
   jr z, rotletencryptLD
   add a, b
   cp a, 'Z' + 2
   jr c , rotletencryptLD
   sub a, 'Z' + 2 - 'A'
rotletencryptLD:
   ld (hl), a
   inc hl
   jr rotletencryptLoop
rotletencryptEnd:
   ld (hl), $00
  pop bc
 pop hl
 ret

;;rotletdecrypt
;;Decrypts a string in rot-n (uses $41 to $5B)
;;Inputs:
;; HL: string pointer
;; A: rot-n
;;Notes:
;; Changes string in place.
rotletdecrypt:
 push hl
  push bc
rotletdecryptMod:
  push hl
   ld h, a
   ld l, 'Z' + 1 - 'A' - 1
   call modHL
   ld b, h
  pop hl
rotletdecryptLoop:
   ld a, (hl)
   cp a, $00
   jr z, rotletdecryptEnd
   cp a, ' '
   jr z, rotletdecryptLD
   sub a, b
   cp a, 'A'
   jr nc , rotletdecryptLD
   add a, 'Z' + 2 - 'A'
rotletdecryptLD:
   ld (hl), a
   inc hl
   jr rotletdecryptLoop
rotletdecryptEnd:
   ld (hl), $00
  pop bc
 pop hl
 ret

;;rotasciiencrypt
;;Encrypts a string in rot-x (uses $00 to $EF)
;;Inputs:
;; HL: string pointer
;; A: x
;;Notes:
;; Changes string in place.
rotasciiencrypt:
 push hl
  push bc
   ld c, 0
   ld c, a
rotasciiencryptLoop:
   ld a, (hl)
   cp 0
   jr z, rotasciiencryptEnd
   add a, c
   cp 0
   jr nz, rotasciiencryptLd
   inc a
rotasciiencryptLd:
   ld (hl), a
   inc hl
   cp a, $EF
   jr c, rotasciiencryptLoop
   dec hl
   sub a, $0F
   ld (hl), a
   inc hl
   jr rotasciiencryptLoop
rotasciiencryptEnd:
   ld (hl), $00
  pop bc
 pop hl
 ret

;;rotasciidecrypt
;;Encrypts a string in rot-x (uses $00 to $EF)
;;Inputs:
;; HL: string pointer
;; A: x
;;Notes:
;; Changes string in place.
rotasciidecrypt:
 push hl
  push bc
   ld c, 0
   ld c, a
rotasciidecryptLoop:
   ld a, (hl)
   cp 0
   jr z, rotasciidecryptEnd
   sub a, c
   cp 0
   jr nz, rotasciidecryptLd
   dec a
rotasciidecryptLd:
   ld (hl), a
   inc hl
   cp a, $EF
   jr c, rotasciidecryptLoop
   dec hl
   add a, $0F
   ld (hl), a
   inc hl
   jr rotasciidecryptLoop
rotasciidecryptEnd:
   ld (hl), $00
  pop bc
 pop hl
 ret
 