;;xorcrypt
;;<en><de>crypts a string using xor
;;Inputs:
;; HL: string pointer
;; DE: seed pointer (shorter)
;;Notes:
;; Changes string in place.
xorcrypt:
 push hl
  push de
   push bc
    call strlen
    push de
     push hl
      ld hl, 0
      ld h, b
      ld l, c
      ex de, hl
       call strlen
      ex de, hl
      ld a, l ;C = len(seed) l=len(string)
      sub a,c
      jp nc, xorcryptNSmaller
      ld c, $01
      ld a, 0
      ld l, 0
xorcryptNSmaller:
      dec c
      jp nc, xorcryptNSmaller2
      ld c, 0
xorcryptNSmaller2:
      ld b, c
      ld c, l ;b=len(seed) c=len(string)
     pop hl
     or a, a
    pop de 
    jr z, xorcryptEnd
    push de
     ex de, hl; hl=seed
    pop hl
    push bc
     push de
      ld de, 0
      ld e, b
      ld bc, 0
      ld c, e
      add hl, bc
     pop de
    pop bc
    inc hl
    ld (hl), %10000000
    push bc
     dec b
     ld a, $FF
     cp a, b
     jr z, xorcryptPadLoopEnd
     ld a, $00
     cp a, b
     jr nz, xorcryptPadLoop
     inc b
xorcryptPadLoop:
     inc hl
     ld (hl), 'e'
     djnz xorcryptPadLoop
xorcryptPadLoopEnd:
    pop bc
    ld (hl), b
    inc hl
    ld (hl), $00
xorcryptEnd:
   pop bc
  pop de
 pop hl
 ;push hl
;xorcryptRandLoop:
  ;ld a, (hl)
  ;or a, a
  ;jr z, xorcryptRandLoopEnd
  ;call randomint
  ;or a, a
  ;jr nz, xorcryptRandLoopLD
  ;ld a, '/'
;xorcryptRandLoopLD:
  ;ld (hl), a
  ;inc hl
  ;jr xorcryptRandLoop
;xorcryptRandLoopEnd:
 ;pop hl
 call strxor
 ret
 
