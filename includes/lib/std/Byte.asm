;;bytecmp
;;Determines if two bytes are equal.
;;Inputs:
;; HL: Byte pointer
;; DE: Byte pointer
;;Outputs:
;; Z: Set if equal, reset if not equal
bytecmp:
 push hl
 push de
 ld a, (de)
 or a
 cp (hl)
 pop de
 pop hl
 ret

;;bytecpy
;; Copies a byte.
;;Inputs:
;; HL: Byte pointer
;; DE: Destination
bytecpy:
 push de
 push hl
 ex de, hl
 ld a, (de)
 ld (hl), a
 pop hl
 pop de
 ret
