;;inputgetkey
;;Gets keycode
;;Outputs:
;; A: keycode
;;Destroys:
;; DE
inputgetkey:
 ex de, hl
  call _GetCSC
 ex de, hl
 or a
 jr z, inputgetkey
 ret

;;inputstr
;;Inputs string of length [Buffer Size] and puts it in (Destination)
;;Inputs:
;; HL: Destination
;; D[E]: Buffer Size 
;;Ouptuts:
;; (HL): String 
inputstr:
 push de
  push hl
   ld (inputstrSizeBuf), de
   push hl
    push de
     ld hl, CurCol
     ld de, inputstrCurColBuf
     call bytecpy
     ld hl, CurRow
     ld de, inputstrCurRowBuf
     call bytecpy
    pop de
   pop hl

   res Shift2nd, (IY + ShiftFlags)
   call _CursorOn

   xor a
   ld b, a
inputstrKeyInput:
   call cursorcurunderreset
   call inputgetkey

   cp sk2nd
   jp z, inputstrKey2nd
   cp skDel
   jp z, inputstrKeyDel
   cp skClear
   jp z, inputstrKeyClear
   cp skEnter
   jp z, inputstrKeyEnter
 
   ld c, a
   ld a, b
   push de
    ld de, (inputstrSizeBuf)
    cp e
   pop de
   jr z, inputstrKeyInput
   ld a, c

   sub skAdd
   jr c, inputstrKeyInput
   cp skMath - skAdd + 1
   jr nc, inputstrKeyInput

   bit Shift2nd, (IY + ShiftFlags) ; TIOS BASED
   call z, chartoascii
   call nz, charnumsymtoascii

   cp $FF
   jr z, inputstrKeyInput

   res textScrolled, (iy+textFlags)

   call  _PutC

   ld (hl), a
   inc hl
   inc b

   bit textScrolled, (iy+textFlags)
   jr z, inputstrKeyInput
   push hl
   ld hl, inputstrCurRowBuf
   ld a, (hl)
   dec a
   ld (hl), a
   pop hl
   jr inputstrKeyInput
inputstrKey2nd:
   push hl
    ld hl, Flags + ShiftFlags
    ld a, (hl)
    xor 1 << Shift2nd     ; Toggle state of Shift2nd flag
    ld (hl), a
   pop hl
   jp inputstrKeyInput 
inputstrKeyDel:
   ld a, b
   cp 0
   jp z, inputstrKeyInput

   ld a, ' '
   call _PutMap

   push hl
    ld hl, CurCol
    dec (hl)
   pop hl

   ld a, ' '
   call _PutMap

   dec hl
   dec b
   jp inputstrKeyInput

inputstrKeyClear:
   ld a, b
   cp 0
   jp z, inputstrKeyInput

   push hl
    push de
     ld hl, inputstrCurColBuf
     ld de, CurCol
     call bytecpy
     ld hl, inputstrCurRowBuf
     ld de, CurRow
     call bytecpy
    pop de
   pop hl

   ld a, ' '
   inc b
inputstrKeyClearLoop:
   call _PutC
   djnz inputstrKeyClearLoop
   ld b, 0

   push de
    push hl
     ld hl, inputstrCurColBuf
     ld de, CurCol
     call bytecpy
     ld hl, inputstrCurRowBuf
     ld de, CurRow
     call bytecpy
    pop hl
   pop de
  pop hl
  push hl
   jp inputstrKeyInput

inputstrKeyEnter:
   ld (hl), 0
   call _CursorOff
   ld a, ' '
   call _PutMap
  pop hl
 pop de
 res Shift2nd, (IY + ShiftFlags)
 ret

inputstrFlags: .db %00000000
; 0: num Flag
inputstrSizeBuf: .dw $0000
inputstrCurColBuf: .db $00
inputstrCurRowBuf: .db $00

;;inputcharstr
;;Inputs string of (only letters) of length [Buffer Size] and puts it in (Destination)
;;Inputs:
;; HL: Destination
;; D[E]: Buffer Size 
;;Ouptuts:
;; (HL): String 
inputcharstr:
 push de
  push hl
   ld (inputcharstrSizeBuf), de
   push hl
    push de
     ld hl, CurCol
     ld de, inputcharstrCurColBuf
     call bytecpy
     ld hl, CurRow
     ld de, inputcharstrCurRowBuf
     call bytecpy
    pop de
   pop hl

   call _CursorOn

   xor a
   ld b, a
inputcharstrKeyInput:
   call cursorcurunderreset
   call inputgetkey

   cp skDel
   jp z, inputcharstrKeyDel
   cp skClear
   jp z, inputcharstrKeyClear
   cp skEnter
   jp z, inputcharstrKeyEnter
 
   ld c, a
   ld a, b
   push de
    ld de, (inputcharstrSizeBuf)
    cp e
   pop de
   jr z, inputcharstrKeyInput
   ld a, c

   sub skAdd
   jr c, inputcharstrKeyInput
   cp skMath - skAdd + 1
   jr nc, inputcharstrKeyInput

   call chartoascii

   cp $FF
   jr z, inputcharstrKeyInput

   res textScrolled, (iy+textFlags)

   call  _PutC

   ld (hl), a
   inc hl
   inc b
  
   bit textScrolled, (iy+textFlags)
   jr z, inputcharstrKeyInput
   push hl
   ld hl, inputcharstrCurRowBuf
   ld a, (hl)
   dec a
   ld (hl), a
   pop hl
   jr inputcharstrKeyInput
inputcharstrKeyDel:
   ld a, b
   cp 0
   jp z, inputcharstrKeyInput

   ld a, ' '
   call _PutMap

   push hl
    ld hl, CurCol
    dec (hl)
   pop hl

   ld a, ' '
   call _PutMap

   dec hl
   dec b
   jp inputcharstrKeyInput

inputcharstrKeyClear:
   ld a, b
   cp 0
   jp z, inputcharstrKeyInput

   push hl
    ld hl, CurRow
    ld c, (hl)
   pop hl

   push hl
    push de
     ld hl, inputcharstrCurColBuf
     ld de, CurCol
     call bytecpy
     ld hl, inputcharstrCurRowBuf
     ld de, CurRow
     call bytecpy
    pop de
   pop hl

   ld a, ' '
   inc b
inputcharstrKeyClearLoop:
   call _PutC
   djnz inputcharstrKeyClearLoop

   push hl
    push de
      ld hl, inputcharstrCurColBuf
     ld de, CurCol
     call bytecpy
    pop de
   pop hl

   push de
    push hl
     ld hl, inputcharstrCurColBuf
     ld de, CurCol
     call bytecpy
     ld hl, inputcharstrCurRowBuf
     ld de, CurRow
     call bytecpy
    pop hl
   pop de
  pop hl
  push hl
   jp inputcharstrKeyInput

inputcharstrKeyEnter:
   ld (hl), 0
   call _CursorOff
   ld a, ' '
   call _PutMap
  pop hl
 pop de
 ret

inputcharstrSizeBuf: .dw $0000
inputcharstrCurColBuf: .db $00
inputcharstrCurRowBuf: .db $00

;;inputnumstr
;;Inputs string of (only numbers) of length [Buffer Size] and puts it in (Destination)
;;Inputs:
;; HL: Destination
;; D[E]: Buffer Size 
;;Ouptuts:
;; (HL): String 
inputnumstr:
 push de
  push hl
   ld (inputnumstrSizeBuf), de
   push hl
    push de
     ld hl, CurCol
     ld de, inputnumstrCurColBuf
     call bytecpy
     ld hl, CurRow
     ld de, inputnumstrCurRowBuf
     call bytecpy
    pop de
   pop hl

   call _CursorOn

   xor a
   ld b, a

inputnumstrKeyInput:
   call cursorcurunderreset
   call inputgetkey

   cp skDel
   jp z, inputnumstrKeyDel
   cp skClear
   jp z, inputnumstrKeyClear
   cp skEnter
   jp z, inputnumstrKeyEnter
 
   ld c, a
   ld a, b
   push de
    ld de, (inputnumstrSizeBuf)
    cp e
   pop de
   jr z, inputnumstrKeyInput
   ld a, c

   sub skAdd
   jr c, inputnumstrKeyInput
   cp skMath - skAdd + 1
   jr nc, inputnumstrKeyInput

   call charnumtoascii
   cp $FF
   jr z, inputnumstrKeyInput

   res textScrolled, (iy+textFlags)

   call  _PutC

   ld (hl), a
   inc hl
   inc b
 
   bit textScrolled, (iy+textFlags)
   jr z, inputnumstrKeyInput
   push hl
   ld hl, inputnumstrCurRowBuf
   ld a, (hl)
   dec a
   ld (hl), a
   pop hl
   jr inputnumstrKeyInput
inputnumstrkeydel:
   ld a, b
   cp 0
   jp z, inputnumstrKeyInput

   ld a, ' '
   call _PutMap

   push hl
     ld hl, CurCol 
     dec (hl)
   pop hl

   ld a, ' '
   call _PutMap

   dec hl
   dec b
   jp inputnumstrKeyInput

inputnumstrKeyClear:
   ld a, b
   cp 0
   jp z, inputnumstrKeyInput

   push hl
    ld hl, CurRow
    ld c, (hl)
   pop hl

   push hl
    push de
     ld hl, inputnumstrCurColBuf
     ld de, CurCol
     call bytecpy
     ld hl, inputnumstrCurRowBuf
     ld de, CurRow
     call bytecpy
    pop de
   pop hl

   ld a, ' '
   inc b
inputnumstrKeyClearLoop:
   call _PutC
   djnz inputnumstrKeyClearLoop

   push de
    push hl
     ld hl, inputnumstrCurColBuf
     ld de, CurCol
     call bytecpy
     ld hl, inputnumstrCurRowBuf
     ld de, CurRow
     call bytecpy
    pop hl
   pop de
  pop hl
  push hl
   jp inputnumstrKeyInput

inputnumstrKeyEnter:
   ld (hl), 0
   call _CursorOff
   ld a, ' '
   call _PutMap
  pop hl
 pop de
 ret

inputnumstrSizeBuf: .dw $0000
inputnumstrCurColBuf: .db $00
inputnumstrCurRowBuf: .db $00
