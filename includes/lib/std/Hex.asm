;;bytetohexstr
;;converts byte to hex string
;;Inputs:
;; A: input byte
;;Outputs:
;; hl: output string location
bytetohexstr:
 push hl
  push bc
   ld bc, 0
   ld c, a
   ld b, $0F
   and a, b
   ld b, a
   ld a, c
   ld c, b
   ld b, 0
   ld hl, hexLUT
   add hl, bc
   ld b, (hl)
   ld hl, bytetohexstrBuf + 1
   ld (hl), b
  pop bc
  push bc
   ld bc, 0
   srl a
   srl a
   srl a
   srl a
   ld b, a
   ld a, c
   ld c, b
   ld b, 0
   ld hl, hexLUT
   add hl, bc
   ld b, (hl)
   ld hl, bytetohexstrBuf
   ld (hl), b
  pop bc
  ld hl, bytetohexstrBuf + 2
  ld (hl), $00
 pop hl
 ld hl, bytetohexstrBuf
 ret
 
bytetohexstrBuf: .db $00, $00, $00, $00

#include "includes/tables/hexLUT.asm"

;;hexToA
;;Converts a hexadecimal string to a number.
;;Inputs:
;; HL: String pointer
;;Outputs:
;; A: Value
hexToA:
 push bc
  push hl
   ld b, 0
_: ld a, (hl)
   or a
   jr z, hexToA_ret
   rl b \ rl b \ rl b \ rl b
   call hexToA_doConvert
   or b
   ld b, a
   inc hl
   jr -_
hexToA_ret:
   ld a, b
  pop hl
 pop bc
 ret

hexToA_doConvert:
 cp 'a' ; Convert to lowercase
 jr c, _
 sub 'a' - 'A'
_: cp 'A' ; Close gap between numbers and letter
 jr c, _
 sub 'A'-('9'+1)
_: sub '0' ; To number
 ret
