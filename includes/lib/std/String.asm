;;strlen
;;Determines the length of a zero delimited string.
;;Inputs:
;; HL: String pointer
;;Outputs:
;; BC: String length
strlen:
 push af
 push hl
  xor a
  ld b, a
  ld c, a
  cpir
  ; bc = -bc
  xor a \ sub c \ ld c, a \ sbc a, a \ sub b \ ld b, a
  dec bc
 pop hl
 pop af
 ret
    
;;strcmp
;;Determines if two strings are equal, and checks alphabetical sort order.
;;Inputs:
;; HL: String pointer
;; DE: String pointer
;;Outputs:
;; Z: Set if equal, reset if not equal
;; C: Set if string HL is alphabetically earlier than string DE
strcmp:
 push hl
 push de
strcmpLoop:
  ld a, (de)
  or a
  jr z, strCmpEnd
  cp (hl)
  jr nz, strcmpExit
  inc hl
  inc de
  jr strcmpLoop
strcmpEnd:
  ld a, (hl)
  or a
strcmpExit:
  ccf
 pop de
 pop hl
 ret

;;strcpy
;;Copies a string.
;;Inputs:
;; HL: String pointer
;; DE: Destination
;;Notes:
;; This will trample into undefined territory if you try to copy a string into some
;;allocated memory it won't fit in.
strcpy:
    push de
    push hl
    ex de, hl
_:  ld a, (de)
    ld (hl), a
    or a
    jr z, _
    inc hl \ inc de
    jr -_
_:  pop hl
    pop de
    ret
    
;;strchr
;;Returns a pointer on the first occurence of a character in a string.
;;Inputs:
;; HL: Haystack
;; B: Needle
;;Outputs:
;; HL: Pointer to first occurence of character
;; Z: Set if character found
;; A: Destroyed
strchr:
strchrLoop:
 ld a, (hl)
 or a
 jr z, strchrNoCharFound
 cp b
 ret z
 inc hl
 jr strchrLoop
strchrNoCharFound:
 inc a
 ret
    
;;strtob
;;Converts a string to a byte
;;Inputs:
;; HL: string pointer
;;Outputs:
;; A: byte
strtob:
 push bc
  push de
   ld c, 0
strtobNumFind:
   ld a, (hl)
   cp $00
   jp z, strtobEXIT
   ld c, 0
   sub '1'
   cp '9' + 1
   jr c, strtobNumWhile
   inc hl
   jr strtobNumFind
strtobNumWhile:
   push hl
strtobNumWhileLoop:
    ld a, c
    cp 3
    jp nc, strtobConvert
    ld a, (hl)
    sub '0' - 1
    jr c, strtobConvert
    cp '9' + 1
    jr nc, strtobConvert
    inc hl
    inc c
    jr strtobNumWhileLoop
strtobConvert:
    dec hl 
    ld a, (hl)
    sub '0'
    ld c, a
    dec hl
   pop de
    call cpHLDE
    jp c, strtobExit  
    ld a, (hl)
    sub '0'
    ld b, 10
    call mulAbyB
    add a, c
    ld c, a
    dec hl
    call cpHLDE
    jp c, strtobExit
    ld a, (hl)
    sub '0'
    ld b, 100
    call mulAbyB
    add a, c
    ld c, a
strtobExit:
   ld a, c
  pop de
 pop bc
 ret 

;;strtow
;;Converts a string to a byte
;;Inputs:
;; HL: string pointer
;;Outputs:
;; HL: byte
strtow:
 push bc
  push de
   ld bc, 0
strtowNumFind:
   ld a, (hl)
   cp $00
   jp z, strtowEXIT
   ld c, 0
   sub '1'
   cp '9' + 1
   jr c, strtowNumWhile
   inc hl
   jr strtowNumFind
strtowNumWhile:
   push hl
strtowNumWhileLoop:
    ld a, c
    cp 5
    jp nc, strtowConvert
    ld a, (hl)
    sub '0' - 1
    jr c, strtowConvert
    cp '9' + 1
    jr nc, strtowConvert
    inc hl
    inc c
    jr strtowNumWhileLoop
strtowConvert:
    ld bc, 0
    dec hl 
    ld a, (hl)
    sub '0'
    ld bc, 0
    ld c, a
    dec hl

    pop de
    call cpHLDE
    jp c, strtowExit  
    ld a, (hl)
    sub '0'
    push hl
     ld hl, 10
     call mulAbyHL
     add hl, bc
     ld b, h
     ld c, l
    pop hl
    dec hl

    call cpHLDE
    jp c, strtowExit  
    ld a, (hl)
    sub '0'
    push hl
     ld hl, 100
     call mulAbyHL
     add hl, bc
     ld b, h
     ld c, l
    pop hl
    dec hl

    call cpHLDE
    jp c, strtowExit  
    ld a, (hl)
    sub '0'
    push hl
     ld hl, 1000
     call mulAbyHL
     add hl, bc
     ld b, h
     ld c, l
    pop hl
    dec hl

    call cpHLDE
    jp c, strtowExit  
    ld a, (hl)
    sub '0'
    push hl
     ld hl, 10000
     call mulAbyHL
     add hl, bc
     ld b, h
     ld c, l
    pop hl

strtowExit:
   ld hl, 0
   ld h, b
   ld l, c
  pop de
 pop bc
 ret

;;strxor
;;xor 2 strings together
;;Inputs:
;; HL: string 1
;; DE: string 2
;;Outputs:
;; HL: Xor String Pointer
strxor:
 push hl
  push de
   ex de, hl
strxorLoop:
   ld a, (de)
   or a, a
   jr z, strxorEnd
   xor (hl)
   or a, a
   jr nz, strxorLoopLD
   ld a, (hl)
strxorLoopLD:
   ld (de), a
   inc de
   inc hl
   jr strxorLoop
strxorEnd:
  pop de
 pop hl
 ret
 
 
;;toLower [Strings]
;;Converts every alpha character of a string to lowercase.
;;Inputs:
;; HL: Pointer to string
;;Notes:
;; This modifies the string in-place.
;toLower:
;    push af \ push hl
;        ld a, (hl)
;        or a
;        jr z, toLowerExit
;        call isAlphaNum
;        jr nc, toLowerNotUpperAlpha
;        cp 'a'
;        jr nc, toLowerNotUpperAlpha
;        add a, 'a' - 'A'
;        ld (hl), a
;toLowerNotUpperAlpha:
;        inc hl
;        jr toLower + 2
;toLowerExit:
;    pop hl \ pop af
;    ret
    
;;toUpper [Strings]
;;Converts every alpha character of a string to uppercase.
;;Inputs:
;; HL: Pointer to string
;;Notes:
;; This modifies the string in-place.
;toUpper:
;    push af \ push hl
;        ld a, (hl)
;        or a
;        jr z, toUpperExit
;        call isAlphaNum
;        jr nc, toUpperNotLowerAlpha
;        cp 'a'
;        jr c, toUpperNotLowerAlpha
;        sub 'a' - 'A'
;        ld (hl), a
;toUpperNotLowerAlpha:
;        inc hl
;        jr toUpper + 2
;toUpperExit:
;    pop hl \ pop af
;    ret
