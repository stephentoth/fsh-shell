;;cursorcharunderreset
;;Sets curUnder to 0
cursorcurunderreset:
 push hl
  ld hl, curUnder
  ld (hl), ' '
 pop hl
 ret
