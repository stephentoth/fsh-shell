;;memset
;;Fills B amount of bytes from hl with a
;;Inputs:
;; HL: start
;; B: how many bytes
;; A: Byte
memset:
 push hl
  push bc
memsetLoop:
   ld (hl), a
   inc hl
   djnz memsetLoop
  pop bc
 pop hl
 ret
