;;cpHLDE
;;Compares HL to DE.
;;Output:
;; Same as z80 CP instruction.
cpHLDE:
 or a
 sbc hl, de
 add hl,de
 ret

;;cpHLBC
;;Compares HL to BC.
;;Output:
;; Same as z80 CP instruction.
cpHLBC:
 or a
 sbc hl, bc
 add hl,bc
 ret

;;cpBCDE
;;Compares DE to BC.
;;Output:
;; Same as z80 CP instruction.
cpBCDE:
 push hl
  ld h, b
  ld l, c
  or a
  sbc hl, de
 pop hl
 ret

;;mulAbyB
;;Multiplys AxB
;;Inputs:
;; A: Multiplier
;; B: Multiplicand
;;Outputs:
;; A: Product of A and B.
mulAbyB:
 push bc
  ld c, a
mulAbyBLoop:
  add a, c
  djnz mulAbyBLoop
  sub c
 pop bc
 ret

;;mulAbyHL
;;Multiplys AxHL
;;Inputs:
;; A: Multiplier
;; HL: Multiplicand
;;Outputs:
;; HL: Product of A and HL.
mulAbyHL:
 push bc
 push de
  ld de, 0
  ex de, hl
  cp 0
  jr z, mulAbyHLEnd
  ld b, a
mulAbyHLLoop:
  add hl, de
  djnz mulAbyHLLoop
mulAbyHLEnd:
 pop de
 pop bc
 ret

;;modHL
;;H % L => H
;;Inputs:
;; H: 
;; L: 
modHL:
 push bc
 ld b, h
 ld a, 0
modHLLoop:
 inc a
 cp a, l
 jr nz, modHLLoopEnd
 ld a, 0
modHLLoopEnd:
 djnz modHLLoop
 ld h, a
 pop bc
 ret
 
 
 
;;mul32By8
;;Performs an unsigned multiplication of DEHL and A.
;;Outputs:
;; DEHL: product of DEHL and A
mul32By8:
 push bc \ push ix
  ld ixl, 8
   push de
    push hl
     ld hl, 0
     ld d, h
     ld e, l
mul32by8loop:
      add hl, hl
      rl e
      rl d
      rla
      jr nc, mul32by8noAdd
     pop bc
     add hl, bc
     ex (sp), hl
     push hl
      adc hl, de
     pop de
     ex de, hl
     ex (sp), hl
     push bc
mul32by8noAdd:
      dec ixl
      jr nz, mul32by8loop
     pop bc
    pop bc
  pop ix \ pop bc
 ret
    
