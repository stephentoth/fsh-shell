;;wordcpy [Words]
;;Copies a word.
;;Inputs:
;; HL: word pointer
;; DE: Destination
wordcpy:
 push de
  push hl
   ex de, hl
   ld a, (de)
   ld (hl), a
   inc de
   inc hl
   ld a, (de)
   ld (hl), a
  pop hl
 pop de
 ret

;;wordcmp
;;Compares 2 words
;;Inputs:
;; HL: word pointer
;; DE: word pointer
wordcmp:
 push hl
  push de
   ld a, (de)
   cp a, (hl)
   jr nz, wordcmpEnd
   inc hl
   inc de
   ld a, (de)
   cp a, (hl)
wordcmpEnd:
  pop de
 pop hl
 ret
