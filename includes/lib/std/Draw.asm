;;drawpixel
;;Sets a pixel to specified color (565)
;;Inputs:
;; HL: color
;; DE: vRam location
drawpixel:
 push hl
 push de
  ex de, hl
  ld (hl), e
  inc hl
  ld (hl), d
 pop de
 pop hl
 ret

;;drawpixelxy
;;Sets a pixel to specified color
;;Inputs:
;; HL: color
;; DE: x
;; A: y
drawpixelxy
 push de
 push hl
  ex de, hl
  add hl, hl
  push hl
   ld hl, 640
   call mulAbyHL
   ex de, hl
  pop hl
  add hl, de
  ld de, vRam
  add hl, de
  ex de, hl
 pop  hl
 call drawpixel
 pop de
 ret

;;drawrow
;;Draws a row on lcd
;;Inputs:
;; A: y
;; HL: color
drawrow:
 push hl
 push bc
  push hl
   ld hl, lcdHeight * 2
   call mulAbyHL
   ld de, vRam
   add hl, de
   ex de, hl
  pop hl
  ld bc, lcdWidth
drawrowLoop:
  call drawpixel
  inc de
  inc de
  dec bc
  push hl
   ld hl, 0
   call cpHLBC
  pop hl
  jr nz, drawrowLoop
 pop bc
 pop hl
 ret

;;drawbar
;;Loops drawing row
;;Inputs:
;; A: y
;; [B]C: height
;; HL: color
drawbar:
 push bc
drawbarLoop:
  call drawrow
  dec bc
  inc a
  push hl
  ld hl, 0
  call cpHLBC
  pop hl
  jr nz, drawbarLoop
 pop bc
 ret

;;drawrectfilled
;;Draws a rectangle from coordinates (Slow)
;;Inputs:
;; DE: x
;; C: y
;; HL: width
;; A: height
;; IXH+IXL: color 
drawrectfilled:
 add hl, de
 add a, c
 ld b, a
drawrectfilledLoopY:
 push de
drawrectfilledLoopX:
  push hl
   push bc
    ex de, hl
     ld d, ixh
     ld e, ixl
    ex de, hl
    ld a, c
    call drawpixelxy
   pop bc
  pop hl
  inc de
  call cpHLDE
  jr nz, drawrectfilledLoopX
 pop de
 inc c
 ld a, b
 cp c
 jr nz, drawrectfilledLoopY
 ret
