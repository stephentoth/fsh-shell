;;array2d
;;gets the address of 2d array
;;Inputs:
;; H: y
;; L: x width 
;; B: x
;; DE: Array start
;;Outputs:
;; DE: Array loc address
array2d:
 push hl
  mlt hl
  add hl, de
  ld de, 0
  ld e, b
  add hl, de
  ex de, hl
 pop hl
 ret
