;;printprompt
;;Sets curcol to 0 and prints prompt
;;Inputs:
;; HL: Prompt String Location
printprompt:
 push hl
  xor a
  ld (CurCol), a
  call _PutS
 pop hl
 ret
