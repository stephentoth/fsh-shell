;;randomint
;;Gets an 8-bit random number.
;;Inputs:
;; A: Seed
;;Outputs:
;; A: Random number (0-255)
;;Notes:
;; This is not cryptographically random.
randomint:
 ld (randomintSeed), a
 push hl
 push de
 push bc
  ld hl, (randomintSeed)
  ld de, (randomintSeed+2)
  ld b, h
  ld c, l
  add hl, hl \ rl e \ rl d
  add hl, hl \ rl e \ rl d
  inc l
  add hl, bc
  ld (randomintSeed), hl
  ld hl, (randomintSeed+2)
  adc hl, de
  ld (randomintSeed+3), hl
  ex de, hl
  ld hl, (randomintSeed)
  ld de, (randomintSeed+4)
  ld bc, 63838
  add hl, hl \ rl c \ rl b
  ld (randomintSeed+6), bc
  sbc a, a
  and %11110001
  xor l
  ld l, a
  ld (randomintSeed+7), hl
  ld a, d
  add a, b
 pop bc
 pop de
 pop hl
 ret
;Actual random bytes
randomintSeed: .db $28, $BE, $B3, $BB, $a3, $f1, $97, $af, $87, $50, $26, $91, $46, $cc, $5f, $c7
