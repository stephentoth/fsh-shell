#include "includes/tables/CSC.asm"
;;chartoascii
;;Converts a char to "ascii"
;;Inputs:
;; A: char code (csc)
;;Outputs:
;; A: ascii char 
chartoascii:
 push hl
 ld hl, 0
 ld h, 0
 ld l, a
 ld de, charMapCSC
 add hl, de
 ld a, (hl)
 pop hl
 ret

;;Charnumtoascii
;;Converts a char to "ascii"
;;Inputs:
;; A: char code (csc)
;;Outputs:
;; A: ascii char 
charnumtoascii:
 push hl
 ld hl, 0
 ld h, 0
 ld l, a
 ld de, numMapCSC
 add hl, de
 ld a, (hl)
 pop hl
 ret

;;charnumsymtoascii
;;Converts a char to "ascii"
;;Inputs:
;; A: char code (csc)
;;Outputs:
;; A: ascii char 
charnumsymtoascii:
 push hl
 ld hl, 0
 ld h, 0
 ld l, a
 ld de, numsymMapCSC
 add hl, de
 ld a, (hl)
 pop hl
 ret
