;;TODO: move module specific macros to diffrent file
;;manroute
;;routes command string to discription
;;Inputs:
;; HL: Command String
;;Outputs:
;; HL: Command Discription String
manroute:
 push de
#macro routestrjp(cmdXs, cmdXm)
  ld de, cmdXs
  call strcmp
  ld de, cmdXm
  jp z, manrouteReturn
#endmacro

#macro subroutestrjp(cmdXs, cmdYs, cmdYm)
  push hl
   ld de, cmdXs
   call strcmp
   ld hl, 0
   jr nz, +_
   ld hl, FshCmdArg1
_:
   ld de, cmdYs
   call strcmp
  pop hl
  ld de, cmdYm
  jp z, manrouteReturn
#endmacro

  call fshcmdmanroute
  call cryptocmdmanroute
  ;Extention Point

  ld hl, FshCmdErrCode
  ld (hl), $01
  ld de, $00
manrouteReturn:
  ex de, hl
 pop de
 ret

;;manrouteu
;;routes command string to usage
;;Inputs:
;; HL: Command String
;;Outputs:
;; HL: Command Usage String
manrouteu:
 push de
#macro routeustrjp(cmdXs, cmdXu)
  ld de, cmdXs
  call strcmp
  ld de, cmdXu
  jp z, manrouteuReturn
#endmacro

#macro subrouteustrjp(cmdXs, cmdYs, cmdYu)
  push hl
   ld de, cmdXs
   call strcmp
   ld hl, 0
   jr nz, +_
   ld hl, FshCmdArg1
_:
   ld de, cmdYs
   call strcmp
  pop hl
  ld de, cmdYu
  jp z, manrouteuReturn
#endmacro

  call fshcmdmanrouteu
  call cryptocmdmanrouteu
  ;Extention Point

  ld hl, FshCmdErrCode
  ld (hl), $01
  ld de, $00
manrouteuReturn:
  ex de, hl
 pop de
 ret
