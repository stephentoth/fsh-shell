.nolist
; Included for Assembler Compatibility
;------------------------------------
#define equ .equ
#define EQU .equ
#define end .end
#define END .end

#define FshVarsSize 128      ;256 bytes   | 64 words
#define FshCmdSize 64        ;64 bytes    | 44 chars , 5 qwords (CMD + args) , + 4 0's| 1 byte null | 2 byte optcode | 1 byte arg flags |1 byte (err code) | 6 unused
#define FshCmdInptSize 44
#define FshCmdCmdSize 8
#define FshScratchSize FshCmdSize - FshCmdInptSize
#define FshPrgmSize 1024

RamVars        equ 0D0EA1Fh   ;saveSScreen | 21945 bytes
FshVars        equ RamVars
FshCmd         equ FshVars + FshVarsSize
FshPrgm        equ FshCmd  + FshCmdSize

FshCmdArg0     equ FshCmd         + 9 
FshCmdArg1     equ FshCmdArg0     + 9
FshCmdArg2     equ FshCmdArg1     + 9
FshCmdArg3     equ FshCmdArg2     + 9
FshCmdArgFlags equ FshCmdArg3     + 10
FshCmdErrCode  equ FshCmdArgFlags + 1
FshCmdOP       equ FshCmdErrCode  + 1
FshScratch     equ FshCmdOP       + 2

.list
