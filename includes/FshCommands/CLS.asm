fshcmdCLS:  .db "CLS", 0
fshcmdCLSo: .dw $0010
fshcmdCLSm: .db "Clears the screen", 0
fshcmdCLSu: .db "CLS", 0
fshcmdCLSf:
 call _ClrLCDFull
 xor  a
 ld (CurRow), a
 ld (CurCol), a
 jp Fin
