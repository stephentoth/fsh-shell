fshcmdCOPY:  .db "COPY", 0
fshcmdCOPYo: .dw $0002
fshcmdCOPYm: .db "Copys a value in FSH memory to another addess", 0
fshcmdCOPYu: .db "COPY <source address> <destination address>", 0
fshcmdCOPYf:
fshcmdCOPYfArg0: 
 ld hl, FshCmdArgFlags
 bit 1, (hl)
 jr nz, fshcmdCOPYfArg1
 bit 0, (hl)
 jr z, fshcmdCOPYfArg0Get
 ld hl, ErrUsageo
 call ErrSet
 call ErrCatch
fshcmdCOPYfArg0Get:
 ld hl, fshcmdCOPYfDSource
 call printprompt
 ld hl, fshcmdCOPYfInputBuffer
 ld de, 3
 call inputnumstr
 call strtob
 ld hl, FshCmdArg0
 ld (hl), a
 call _NewLine
fshcmdCOPYfArg1:
 ld hl, FshCmdArgFlags
 bit 3, (hl)
 jr nz, fshcmdCOPYfExe
 bit 2, (hl)
 jr z, fshcmdCOPYfArg1Get
 ld hl, ErrUsageo
 call ErrSet
 call ErrCatch
fshcmdCOPYfArg1Get:
 ld hl, fshcmdCOPYfDSource
 call printprompt
 ld hl, fshcmdCOPYfInputBuffer
 ld de, 3
 call inputnumstr
 call strtob
 ld hl, FshCmdArg1
 ld (hl), a
 call _NewLine
fshcmdCOPYfExe:
 ld hl, FshCmdArg0
 ld de, 0
 ld e, (hl)
 ex de, hl
 push hl
  ld hl, FshCmdArg1
  ld de, 0
  ld e, (hl)
 pop hl
 call fshvarcpy
 call ErrCatch 
 jp Fin

fshcmdCOPYfInputBuffer: .db $00, $00, $00, $00
fshcmdCOPYfDSource: .db "Source= ", 0
fshcmdCOPYfDDestination: .db "Destination= ", 0
