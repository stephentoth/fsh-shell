fshcmdHELP:  .db "HELP", 0
fshcmdHELPo: .dw $0021
fshcmdHELPm: .db "Displays all commands", 0
fshcmdHELPu: .db "HELP", 0
fshcmdHELPf:
 ld hl, FshCmdArgFlags
 bit 0, (hl)
 jr z, fshcmdHELPfExe
 ld hl, ErrUsageo
 call ErrSet
 call ErrCatch
fshcmdHELPfExe:
 ld hl, fshcmdHELPfLine1
 call _PutS
 call _NewLine

#macro fshcmdHELPmRow1(cmdXs)
 xor a
 ld (curCol), a
 ld hl, cmdXs
 call _PutS
 call _NewLine
#endmacro
#macro fshcmdHELPmRow2(cmdXs, cmdYs)
 xor a
 ld (curCol), a
 ld hl, cmdXs
 call _PutS
 ld a, 9
 ld (curcol), a
 ld hl, cmdYs
 call _PutS
 call _NewLine
#endmacro
#macro fshcmdHELPmRow3(cmdXs, cmdYs, cmdZs)
 xor a
 ld (curCol), a
 ld hl, cmdXs
 call _PutS
 ld a, 9
 ld (curcol), a
 ld hl, cmdYs
 call _PutS
 ld a, 19
 ld (curcol), a
 ld hl, cmdZs
 call _PutS
 call _NewLine
#endmacro

 fshcmdHELPmRow3(fshcmdEXIT, fshcmdSET, fshcmdCOPY)
 fshcmdHELPmRow3(fshcmdMOVE, fshcmdCLS, fshcmdVAR)
 fshcmdHELPmRow3(fshcmdVARS, fshcmdSLEEP, fshcmdINFO)
 fshcmdHELPmRow2(fshcmdHELP, fshcmdMAN)
 fshcmdHELPmRow2(cryptocmdENCRYPT, cryptocmdDECRYPT)
 ; Extention Point

 call _NewLine
 ld hl, fshcmdHELPfLine2
 call _PutS
 call _NewLine 
 jp Fin

fshcmdHELPfLine1: .db "Available Commands:", 0
fshcmdHELPfLine2: .db "Type 'MAN' and command for more info", 0
