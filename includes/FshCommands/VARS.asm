fshcmdVARS:  .db "VARS", 0
fshcmdVARSo: .dw $0012
fshcmdVARSm: .db "Displays all Fsh Vars", 0
fshcmdVARSu: .db "VARS", 0
fshcmdVARSf:
 ld hl, FshCmdArgFlags
 bit 0, (hl)
 jr z, fshcmdVARSfExe
 ld hl, ErrUsageo
 call ErrSet
 call ErrCatch
fshcmdVARSfExe:
  ld b, 0
  ld c, 0
  ld de, 0
fshcmdVARSfLoop:
  call fshvarget
  ld a, h
  call _PutC
  ld a, l
  call _PutC
  ld a, c
  inc c
  cp 7
  jr nz, fshcmdVARSfLoopU
  push hl
   push bc
    push de
     call _NewLine
    pop de
   pop bc
  pop hl
  ld c, 0
  jr fshcmdVARSfLoopEnd
fshcmdVARSfLoopU:
  ld a, '_'
  call _PutC
fshcmdVARSfLoopEnd:
  inc b
  inc de
  ld a, b
  cp FshVarsSize / 2
  jr c, fshcmdVARSfLoop 
  jp Fin 
