fshcmdMOVE:  .db "MOVE", 0
fshcmdMOVEo: .dw $0003
fshcmdMOVEm: .db "Move a value in FSH memory to another addess", 0
fshcmdMOVEu: .db "MOVE <source address> <destination address>", 0
fshcmdMOVEf:
fshcmdMOVEfArg0: 
 ld hl, FshCmdArgFlags
 bit 1, (hl)
 jr nz, fshcmdMOVEfArg1
 bit 0, (hl)
 jr z, fshcmdMOVEfArg0Get
 ld hl, ErrUsageo
 call ErrSet
 call ErrCatch
fshcmdMOVEfArg0Get:
 ld hl, fshcmdMOVEfDSource
 call printprompt
 ld hl, fshcmdMOVEfInputBuffer
 ld de, 3
 call inputnumstr
 call strtob
 ld hl, FshCmdArg0
 ld (hl), a
 call _NewLine
fshcmdMOVEfArg1:
 ld hl, FshCmdArgFlags
 bit 3, (hl)
 jr nz, fshcmdMOVEfExe
 bit 2, (hl)
 jr z, fshcmdMOVEfArg1Get
 ld hl, ErrUsageo
 call ErrSet
 call ErrCatch
fshcmdMOVEfArg1Get:
 ld hl, fshcmdMOVEfDDestination
 call printprompt
 ld hl, fshcmdMOVEfInputBuffer
 ld de, 3
 call inputnumstr
 call strtob
 ld hl, FshCmdArg1
 ld (hl), a
 call _NewLine
fshcmdMOVEfExe:
 ld hl, FshCmdArg0
 ld de, 0
 ld e, (hl)
 ex de, hl
 push hl
  ld hl, FshCmdArg1
  ld de, 0
  ld e, (hl)
 pop hl
 call fshvarcpy
 ex de, hl
 ld hl, 0
 call fshvarset
 call ErrCatch
 jp Fin

fshcmdMOVEfInputBuffer: .db $00, $00, $00, $00
fshcmdMOVEfDSource: .db "Source= ", 0
fshcmdMOVEfDDestination: .db "Destination= ", 0
