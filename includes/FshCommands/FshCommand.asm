fshcmddecode:
 decodecmdopt(fshcmdEXIT, fshcmdEXITo)
 decodecmdopt(fshcmdSET, fshcmdSETo)
 decodecmdopt(fshcmdCOPY, fshcmdCOPYo)
 decodecmdopt(fshcmdMOVE, fshcmdMOVEo)
 decodecmdopt(fshcmdCLS, fshcmdCLSo)
 decodecmdopt(fshcmdVAR, fshcmdVARo)
 decodecmdopt(fshcmdVARS, fshcmdVARSo)
 decodecmdopt(fshcmdSLEEP, fshcmdSLEEPo)
 decodecmdopt(fshcmdINFO, fshcmdINFOo)
 decodecmdopt(fshcmdHELP, fshcmdHELPo)
 decodecmdopt(fshcmdMAN, fshcmdMANo)
 ret

fshcmdroute:
 pop bc ;pop the ret loc from stack into bc
 routeoptjp(fshcmdEXITo, fshcmdEXITf)
 routeoptjp(fshcmdSETo, fshcmdSETf)
 routeoptjp(fshcmdCOPYo, fshcmdCOPYf)
 routeoptjp(fshcmdMOVEo, fshcmdMOVEf)
 routeoptjp(fshcmdCLSo, fshcmdCLSf)
 routeoptjp(fshcmdVARo, fshcmdVARf)
 routeoptjp(fshcmdVARSo, fshcmdVARSf)
 routeoptjp(fshcmdSLEEPo, fshcmdSLEEPf)
 routeoptjp(fshcmdINFOo, fshcmdINFOf) 
 routeoptjp(fshcmdHELPo, fshcmdHELPf) 
 routeoptjp(fshcmdMANo, fshcmdMANf) 
 push bc ;put ret location back on stack
 ret

fshcmdmanroute:
 pop bc
  routestrjp(fshcmdEXIT, fshCmdEXITm)
  routestrjp(fshcmdSET, fshCmdSETm)
  routestrjp(fshcmdCOPY, fshCmdCOPYm)
  routestrjp(fshcmdMOVE, fshCmdMOVEm)
  routestrjp(fshcmdCLS, fshCmdCLSm)
  routestrjp(fshcmdVAR, fshCmdVARm)
  routestrjp(fshcmdVARS, fshCmdVARSm)
  routestrjp(fshcmdSLEEP, fshCmdSLEEPm)
  routestrjp(fshcmdINFO, fshCmdINFOm)
  routestrjp(fshcmdHELP, fshCmdHELPm)
  routestrjp(fshcmdMAN, fshCmdMANm) 
 push bc
 ret

fshcmdmanrouteu:
 pop bc
  routeustrjp(fshcmdEXIT, fshCmdEXITu)
  routeustrjp(fshcmdSET, fshCmdSETu)
  routeustrjp(fshcmdCOPY, fshCmdCOPYu)
  routeustrjp(fshcmdMOVE, fshCmdMOVEu)
  routeustrjp(fshcmdCLS, fshCmdCLSu)
  routeustrjp(fshcmdVAR, fshCmdVARu)
  routeustrjp(fshcmdVARS, fshCmdVARSu)
  routeustrjp(fshcmdSLEEP, fshCmdSLEEPu)
  routeustrjp(fshcmdINFO, fshCmdINFOu)
  routeustrjp(fshcmdHELP, fshCmdHELPu)
  routeustrjp(fshcmdMAN, fshCmdMANu)
 push bc
 ret

#include "includes/FshCommands/FshCommands.inc"
