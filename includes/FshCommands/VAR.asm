fshcmdVAR:  .db "VAR", 0
fshcmdVARo: .dw $0011
fshcmdVARm: .db "Displays a value from FSH memory", 0
fshcmdVARu: .db "VAR <source address>", 0
fshcmdVARf:
fshcmdVARfArg0: 
 ld hl, FshCmdArgFlags
 bit 1, (hl)
 jr nz, fshcmdVARfExe
 bit 0, (hl)
 jr z, fshcmdVARfArg0Get
 ld hl, ErrUsageo
 call ErrSet
 call ErrCatch
fshcmdVARfArg0Get:
 ld hl, fshcmdVARfDIndex
 call printprompt
 ld hl, fshcmdVARfInputBuffer
 ld de, 3
 call inputnumstr
 call strtow
 ld de, FshCmdArg0
 ex de, hl
 ld (hl), e
 inc hl
 ld (hl), d 
 call _NewLine
fshcmdVARfExe:
 ld hl, FshCmdArg0
 ld de, 0
 ld e, (hl)
 inc hl
 ld d, (hl)
 call fshvarget
 call ErrCatch
 call _DispHL_s
 call _NewLine
 jp Fin 

fshcmdVARfInputBuffer: .db $00, $00, $00, $00
fshcmdVARfDIndex: .db "Index= ", 0
