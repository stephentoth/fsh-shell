fshcmdSET:  .db "SET", 0
fshcmdSETo: .dw $0001
fshcmdSETm: .db "Sets a value in FSH memory", 0
fshcmdSETu: .db "SET <destination address> <value>", 0
fshcmdSETf:
fshcmdSETfArg0: 
 ld hl, FshCmdArgFlags
 bit 1, (hl)
 jr nz, fshcmdSETfArg1
 bit 0, (hl)
 jr z, fshcmdSETfArg0Get
 ld hl, ErrUsageo
 call ErrSet
 call ErrCatch
fshcmdSETfArg0Get:
 ld hl, fshcmdSETfDIndex
 call printprompt
 ld hl, fshcmdSETfInputBuffer
 ld de, 3
 call inputnumstr
 call strtow
 ld de, FshCmdArg0
 ex de, hl
 ld (hl), e
 inc hl
 ld (hl), d 
 call _NewLine
fshcmdSETfArg1:
 ld hl, FshCmdArgFlags
 bit 3, (hl)
 jr nz, fshcmdSETfExe
 bit 2, (hl)
 jr z, fshcmdSETfArg1Get
 ld hl, ErrUsageo
 call ErrSet
 call ErrCatch
fshcmdSETfArg1Get:
 ld hl, fshcmdSETfDValue
 call printprompt
 ld hl, fshcmdSETfInputBuffer
 ld de, 5
 call inputnumstr
 call strtow
 ex de, hl
 ld hl, FshCmdArg1
 ld (hl), e
 inc hl
 ld (hl), d
 call _NewLine
fshcmdSETfExe:
 ld hl, FshCmdArg0
 ld de, 0
 ld e, (hl)
 inc hl
 ld d, (hl)
 push de
  ld hl, FshCmdArg1
  ld e, (hl)
  inc hl
  ld d, (hl)
  ex de, hl
 pop de
 call fshvarset
 call ErrCatch
 jp Fin

fshcmdSETfInputBuffer: .db $00, $00, $00, $00, $00, $00
fshcmdSETfDIndex: .db "Destination= ", 0
fshcmdSETfDValue: .db "Value= ", 0
