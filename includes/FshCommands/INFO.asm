fshcmdINFO:  .db "INFO", 0
fshcmdINFOo: .dw $0020
fshcmdINFOm: .db "Displays Syetem Information and Stats", 0
fshcmdINFOu: .db "INFO", 0
fshcmdINFOf:
 ld hl, FshCmdArgFlags
 bit 0, (hl)
 jr z, fshcmdINFOfExe
 ld hl, ErrUsageo
 call ErrSet
 call ErrCatch
fshcmdINFOfExe:
 call _ClrLCDFull
 xor a
 ld (CurRow), a
 ld (CurCol), a
 
 call printfshnamelong
 call _NewLine

 ld hl, FshBy
 call _PutS
 call _NewLine

 ld hl, FshSite
 call _PutS
 call _NewLine

 call _NewLine
 ld hl, fshcmdINFOfSPS1
 call _PutS
 ld a, $27
 call _PutC
 ld hl, PS1
 call _PutS
 ld a, $27
 call _PutC
 call _NewLine

 jp Fin

fshcmdINFOfSPS1: .db "PS1", $CE, $CE, $CE, ":", 0 
