fshcmdMAN:  .db "MAN", 0
fshcmdMANo: .dw $0022
fshcmdMANm: .db "Displays MAN text for command", 0
fshcmdMANu: .db "MAN <command>", 0
fshcmdMANf:
fshcmdMANfCheckArgs: 
 ld hl, FshCmdArgFlags
 bit 0, (hl)
 jr z, fshcmdMANfArg0Get
 bit 1, (hl)
 jr z, fshcmdMANfExe
 ld hl, ErrUsageo
 call ErrSet
 call ErrCatch
fshcmdMANfArg0Get:
 ld hl, fshcmdMANfPrompt
 call printprompt

 ld hl, FshCmdArg0
 ld de, FshCmdCmdSize
 call inputstr
 call _NewLine

fshcmdMANfExe:
 ld hl, FshCmdArg0
 ld de, FshCmd
 call strcpy

 ld hl, fshcmdMANfPrefix
 call _PutS

 ld hl, FshCmd
 call _PutS
 call _NewLine
 call _NewLine

 ld hl, FshCmd
 call manroute
 ld de, $00
 call cpHLDE
 jp z, Err

 call _PutS
 call _NewLine
 call _NewLine

 ld hl, fshcmdMANfUsage
 call _PutS

 ld hl, FshCmd
 call manrouteu
 ld de, $00
 call cpHLDE
 jp z, Err
 call _PutS
 call _NewLine

 jp Fin

fshcmdMANfPrefix: .db "MAN-DB", $DF, ' ', 0
fshcmdMANfUsage:  .db "Usage: ",0
fshcmdMANfPrompt: .db "Command= ", 0
