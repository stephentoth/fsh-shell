fshcmdSLEEP:  .db "SLEEP", 0
fshcmdSLEEPo: .dw $0016
fshcmdSLEEPm: .db "Puts the calculator into sleep mode (APD)", 0
fshcmdSLEEPu: .db "SLEEP", 0
fshcmdSLEEPf:
 di
  call _EnableAPD
  ld a,1
  ld hl,apdSubTimer
  ld (hl),a
  inc hl 
  ld (hl),a
  set apdRunning,(iy+apdFlags)
 ei
 jp Fin
