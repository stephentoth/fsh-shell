cryptocmdENCRYPT:  .db "ENCRYPT", 0
cryptocmdENCRYPTo: .dw $0201
cryptocmdENCRYPTm: .db "Encrypts a string using a cypher. see 'ENCRYPT HELP' for info on cyphers", 0
cryptocmdENCRYPTu: .db "ENCRYPT <Crypt Command> <Crypt Args>", 0
cryptocmdENCRYPTf:
cryptocmdENCRYPTfArg0: 
 ld hl, FshCmdArgFlags
 bit 0, (hl)
 jr z, cryptocmdENCRYPTfArg0Get
 bit 1, (hl)
 jr z, cryptocmdENCRYPTfExe
 ld hl, ErrUsageo
 call ErrSet
 call ErrCatch
cryptocmdENCRYPTfArg0Get:
 ld hl, cryptocmdENCRYPTfSCypher
 call printprompt
 ld hl, FshCmdArg0
 ld de, FshCmdInptSize
 call inputstr
 call _NewLine
cryptocmdENCRYPTfExe:

#macro cryptocmdENCRYPTcmpjp(cmdX, cmdXf)
 ld hl, FshCmdArg0
 ld de, CmdX
 call strcmp
 jp z, cmdXf
#endmacro

 cryptocmdENCRYPTcmpjp(cryptocmdROT, cryptocmdROTf)
 cryptocmdENCRYPTcmpjp(cryptocmdXOR, cryptocmdXORf)
 cryptocmdENCRYPTcmpjp(cryptocmdHELP, cryptocmdHELPf)
 
 ld hl, FshCmdArg0
 ld de, FshCmd
 call strcpy
 
 ld hl, ErrCmdNotFoundo
 ld de, FshCmdErrCode
 call bytecpy
 call ErrSet
 jp Err

cryptocmdENCRYPTfSCypher: .db "Cypher= ", 0
