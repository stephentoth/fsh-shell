cryptocmdunXOR:  .db "XOR", 0
cryptocmdunXORm: .db "XOR: Cypher, and decodes a string", 0
cryptocmdunXORu: .db "DECRYPT XOR <key>", 0
cryptocmdunXORf:
cryptocmdunXORfArg1: 
 ld hl, FshCmdArgFlags
 bit 2, (hl)
 jr z, cryptocmdunXORfArg1Get
 bit 3, (hl)
 jr z, cryptocmdunXORfArg2
 ld hl, ErrUsageo
 call ErrSet
 call ErrCatch
cryptocmdunXORfArg1Get:
 ld hl, cryptocmdunXORfDKey
 call printprompt
 ld hl, FshCmdArg1
 ld de, FshCmdCmdSize
 call inputstr
 call _NewLine
cryptocmdunXORfArg2:
 ld hl, FshCmdArg2
 ld de, FshScratch
 call strcpy
 ld hl, FshCmdArgFlags
 bit 4, (hl)
 jr nz, cryptocmdunXORfExe
cryptocmdunXORfArg2Get:
 ld hl, cryptocmdunXORfStringPBuffer
 ld b, $0F
 ld a, $00
 call memset
 ld hl, cryptocmdunXORfDMessage
 call printprompt
 ld hl, FshScratch
 ld de, FshScratchSize - 4
 call inputstr
 ld bc, cryptocmdunXORfStringPBuffer
cryptocmdunXORfArg2GetLoop:
 ld a, (hl)
 or a, a
 jr z, cryptocmdunXORfArg2GetLoopEnd
 ld (cryptocmdunXORfStringBuffer), a
 inc hl
 ld a, (hl)
 ld (cryptocmdunXORfStringBuffer + 1), a
 push hl
  ld hl, cryptocmdunXORfStringBuffer
  call hexToA
 pop hl
 ld (bc), a
 inc hl
 inc bc
 jr cryptocmdunXORfArg2GetLoop
cryptocmdunXORfArg2GetLoopEnd: 
 call _NewLine
cryptocmdunXORfExe:
 ld hl, cryptocmdunXORfStringPBuffer
 ld de, FshCmdArg1
 call xorcrypt
 ld hl, cryptocmdunXORfDResult
 call _PutS
 ld hl, cryptocmdunXORfStringPBuffer
 call _PutS
 call _NewLine
 jp Fin

cryptocmdunXORfDKey: .db "Key= ", 0
cryptocmdunXORfDMessage: .db "CypherText= ", 0
cryptocmdunXORfDResult: .db "Result: ", 0
cryptocmdunXORfStringBuffer: .db $00, $00, $00, $00
cryptocmdunXORfStringPBuffer: .db $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00
