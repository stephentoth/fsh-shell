cryptocmddecode:
 decodecmdopt(cryptocmdENCRYPT, cryptocmdENCRYPTo)
 decodecmdopt(cryptocmdDECRYPT, cryptocmdDECRYPTo)
 ret

cryptocmdroute:
 pop bc
  routeoptjp(cryptocmdENCRYPTo, cryptocmdENCRYPTf)
  routeoptjp(cryptocmdDECRYPTo, cryptocmdDECRYPTf)
 push bc
 ret

cryptocmdmanroute:
 pop bc
  subroutestrjp(cryptocmdENCRYPT, cryptocmdROT, cryptocmdROTm)
  subroutestrjp(cryptocmdDECRYPT, cryptocmdunROT, cryptocmdunROTm)
  
  subroutestrjp(cryptocmdENCRYPT, cryptocmdXOR, cryptocmdXORm)
  subroutestrjp(cryptocmdDECRYPT, cryptocmdunXOR, cryptocmdunXORm)
  
  subroutestrjp(cryptocmdENCRYPT, cryptocmdHELP, cryptocmdHELPm)
  subroutestrjp(cryptocmdDECRYPT, cryptocmdunHELP, cryptocmdunHELPm)
  
  routestrjp(cryptocmdENCRYPT, cryptocmdENCRYPTm)
  routestrjp(cryptocmdDECRYPT, cryptocmdDECRYPTm)
  
 push bc
 ret


cryptocmdmanrouteu:
 pop bc
  subrouteustrjp(cryptocmdENCRYPT, cryptocmdROT, cryptocmdROTu)
  subrouteustrjp(cryptocmdDECRYPT, cryptocmdunROT, cryptocmdunROTu)

  subrouteustrjp(cryptocmdENCRYPT, cryptocmdXOR, cryptocmdXORu)
  subrouteustrjp(cryptocmdDECRYPT, cryptocmdunXOR, cryptocmdunXORu)
  
  subrouteustrjp(cryptocmdENCRYPT, cryptocmdHELP, cryptocmdHELPu)
  subrouteustrjp(cryptocmdDECRYPT, cryptocmdunHELP, cryptocmdunHELPu)
  
  routeustrjp(cryptocmdENCRYPT, cryptocmdENCRYPTu)
  routeustrjp(cryptocmdDECRYPT, cryptocmdDECRYPTu)
  
 push bc
 ret

#include "includes/CryptoCommands/CryptoCommands.inc"
