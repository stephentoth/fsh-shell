cryptocmdROT:  .db "ROT", 0
cryptocmdROTm: .db "ROT: Cypher, rotates the alphabet and encodes a string", 0
cryptocmdROTu: .db "ENCRYPT ROT <shift#> <message>", 0
cryptocmdROTf:
cryptocmdROTfArg1: 
 ld hl, FshCmdArgFlags
 bit 3, (hl)
 jr nz, cryptocmdROTfArg2
 bit 2, (hl)
 jr z, cryptocmdROTfArg1Get
 ld hl, ErrUsageo
 call ErrSet
 call ErrCatch
cryptocmdROTfArg1Get:
 ld hl, cryptocmdROTfDShift
 call printprompt
 ld hl, FshCmdArg1
 ld de, 3
 call inputnumstr
 call strtow
 ld de, FshCmdArg1
 ex de, hl
 ld (hl), e
 call _NewLine
cryptocmdROTfArg2:
 ld hl, FshCmdArg2
 ld de, FshScratch
 call strcpy
 ld hl, FshCmdArgFlags
 bit 4, (hl)
 jr z, cryptocmdROTfArg2Get
 bit 5, (hl)
 jr z, cryptocmdROTfExe
 ld hl, ErrUsageo
 call ErrSet
 call ErrCatch
cryptocmdROTfArg2Get:
 ld hl, cryptocmdROTfDMessage
 call printprompt
 ld hl, FshScratch
 ld de, FshScratchSize - 4
 call inputcharstr
 call _NewLine
cryptocmdROTfExe:
 ld hl, FshCmdArg1
 ld a, (hl)
 ld hl, FshScratch
 call rotletencrypt
 push hl
  ld hl, cryptocmdROTfDResult
  call _PutS
 pop hl
 call _PutS
 call _NewLine
 jp Fin

cryptocmdROTfDShift: .db "Shift= ", 0
cryptocmdROTfDMessage: .db "Message= ", 0
cryptocmdROTfDResult: .db "Result: ", 0
