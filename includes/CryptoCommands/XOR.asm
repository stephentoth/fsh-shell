cryptocmdXOR:  .db "XOR", 0
cryptocmdXORm: .db "XOR: Cypher, and encodes a string", 0
cryptocmdXORu: .db "ENCRYPT XOR <key> <message>", 0
cryptocmdXORf:
cryptocmdXORfArg1: 
 ld hl, FshCmdArgFlags
 bit 2, (hl)
 jr z, cryptocmdXORfArg1Get
 bit 3, (hl)
 jr z, cryptocmdXORfArg2
 ld hl, ErrUsageo
 call ErrSet
 call ErrCatch
cryptocmdXORfArg1Get:
 ld hl, cryptocmdXORfDKey
 call printprompt
 ld hl, FshCmdArg1
 ld de, FshCmdCmdSize
 call inputstr
 call _NewLine
cryptocmdXORfArg2:
 ld hl, FshCmdArg2
 ld de, FshScratch
 call strcpy
 ld hl, FshCmdArgFlags
 bit 4, (hl)
 jr z, cryptocmdXORfArg2Get
 bit 5, (hl)
 jr z, cryptocmdXORfExe
 ld hl, ErrUsageo
 call ErrSet
 call ErrCatch
cryptocmdXORfArg2Get:
 ld hl, cryptocmdXORfDMessage
 call printprompt
 ld hl, FshScratch
 ld de, FshScratchSize - 1 / 2
 call inputcharstr
 call _NewLine
cryptocmdXORfExe:
 ld hl, FshScratch
 ld de, FshCmdArg1
 call xorcrypt
 ld hl, cryptocmdXORfDResult
 call _PutS
 ld hl, FshScratch
cryptocmdXORfPrint:
 ld a, (hl)
 or a, a
 jr z, cryptocmdXORfPrintEnd
 push hl
  call bytetohexstr
  ld a, (hl)
  call _PutC
  inc hl
  ld a, (hl)
  call _PutC
 pop hl
 inc hl
 jr cryptocmdXORfPrint
cryptocmdXORfPrintEnd:
 call _NewLine
 jp Fin

cryptocmdXORfDKey: .db "Key= ", 0
cryptocmdXORfDMessage: .db "Message= ", 0
cryptocmdXORfDResult: .db "Result: ", 0
