cryptocmdunHELP:  .db "HELP", 0
cryptocmdunHELPm: .db "Help for cyphers", 0
cryptocmdunHELPu: .db "DECRYPT HELP", 0
cryptocmdunHELPf:
 ld hl, FshCmdArgFlags
 bit 2, (hl)
 jr z, cryptocmdunHELPfExe
 ld hl, ErrUsageo
 call ErrSet
 call ErrCatch
cryptocmdunHELPfExe:
 ld hl, cryptocmdunHELPfLine1
 call _PutS
 call _NewLine

#macro cryptocmdunHELPmRow1(cmdXs)
 xor a
 ld (curCol), a
 ld hl, cmdXs
 call _PutS
 call _NewLine
#endmacro
#macro cryptocmdunHELPmRowU(cmdXs, cmdXu)
 xor a
 ld (curCol), a
 ld hl, cmdXs
 call _PutS
 ld a, ':'
 call _PutC
 ld a, ' '
 call _PutC
 ld hl, cmdXu
 call _PutS
 call _NewLine
#endmacro
#macro cryptocmdunHELPmRow2(cmdXs, cmdYs)
 xor a
 ld (curCol), a
 ld hl, cmdXs
 call _PutS
 ld a, 9
 ld (curcol), a
 ld hl, cmdYs
 call _PutS
 call _NewLine
#endmacro
#macro cryptocmdunHELPmRow3(cmdXs, cmdYs, cmdZs)
 xor a
 ld (curCol), a
 ld hl, cmdXs
 call _PutS
 ld a, 9
 ld (curcol), a
 ld hl, cmdYs
 call _PutS
 ld a, 19
 ld (curcol), a
 ld hl, cmdZs
 call _PutS
 call _NewLine
#endmacro

 cryptocmdunHELPmRowU(cryptocmdunROT, cryptocmdunROTu)
 cryptocmdunHELPmRowU(cryptocmdunXOR, cryptocmdunXORu)

 call _NewLine
 jp Fin

cryptocmdunHELPfLine1: .db "Available Cryptos:", 0
