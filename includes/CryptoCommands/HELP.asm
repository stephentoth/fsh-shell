cryptocmdHELP:  .db "HELP", 0
cryptocmdHELPm: .db "Help for cyphers", 0
cryptocmdHELPu: .db "ENCRYPT HELP", 0
cryptocmdHELPf:
 ld hl, FshCmdArgFlags
 bit 2, (hl)
 jr z, cryptocmdHELPfExe
 ld hl, ErrUsageo
 call ErrSet
 call ErrCatch
cryptocmdHELPfExe:
 ld hl, cryptocmdHELPfLine1
 call _PutS
 call _NewLine

#macro cryptocmdHELPmRow1(cmdXs)
 xor a
 ld (curCol), a
 ld hl, cmdXs
 call _PutS
 call _NewLine
#endmacro
#macro cryptocmdHELPmRowU(cmdXs, cmdXu)
 xor a
 ld (curCol), a
 ld hl, cmdXs
 call _PutS
 ld a, ':'
 call _PutC
 ld a, ' '
 call _PutC
 ld hl, cmdXu
 call _PutS
 call _NewLine
#endmacro
#macro cryptocmdHELPmRow2(cmdXs, cmdYs)
 xor a
 ld (curCol), a
 ld hl, cmdXs
 call _PutS
 ld a, 9
 ld (curcol), a
 ld hl, cmdYs
 call _PutS
 call _NewLine
#endmacro
#macro cryptocmdHELPmRow3(cmdXs, cmdYs, cmdZs)
 xor a
 ld (curCol), a
 ld hl, cmdXs
 call _PutS
 ld a, 9
 ld (curcol), a
 ld hl, cmdYs
 call _PutS
 ld a, 19
 ld (curcol), a
 ld hl, cmdZs
 call _PutS
 call _NewLine
#endmacro

 cryptocmdHELPmRowU(cryptocmdROT, cryptocmdROTu)
 cryptocmdHELPmRowU(cryptocmdXOR, cryptocmdXORu)

 call _NewLine
 jp Fin

cryptocmdHELPfLine1: .db "Available Cryptos:", 0
