cryptocmdDECRYPT:  .db "DECRYPT", 0
cryptocmdDECRYPTo: .dw $0202
cryptocmdDECRYPTm: .db "Decrypts a string using a cypher. see 'DECRYPT HELP' for info on cyphers", 0
cryptocmdDECRYPTu: .db "DECRYPT <Crypt Command> <Crypt Args>", 0
cryptocmdDECRYPTf:
cryptocmdDECRYPTfArg0: 
 ld hl, FshCmdArgFlags
 bit 0, (hl)
 jr z, cryptocmdDECRYPTfArg0Get
 bit 1, (hl)
 jr z, cryptocmdDECRYPTfExe
 ld hl, ErrUsageo
 call ErrSet
 call ErrCatch
cryptocmdDECRYPTfArg0Get:
 ld hl, cryptocmdDECRYPTfSCypher
 call printprompt
 ld hl, FshCmdArg0
 ld de, FshCmdInptSize
 call inputstr
 call _NewLine
cryptocmdDECRYPTfExe:

#macro cryptocmdDECRYPTcmpjp(cmdX, cmdXf)
 ld hl, FshCmdArg0
 ld de, CmdX
 call strcmp
 jp z, cmdXf
#endmacro

 cryptocmdDECRYPTcmpjp(cryptocmdunROT, cryptocmdunROTf)
 cryptocmdDECRYPTcmpjp(cryptocmdunXOR, cryptocmdunXORf)
 cryptocmdDECRYPTcmpjp(cryptocmdunHELP, cryptocmdunHELPf)
 
 ld hl, FshCmdArg0
 ld de, FshCmd
 call strcpy
 
 ld hl, ErrCmdNotFoundo
 ld de, FshCmdErrCode
 call bytecpy
 call ErrSet
 jp Err

cryptocmdDECRYPTfSCypher: .db "Cypher= ", 0
