#include "includes/CryptoCommands/ENCRYPT.asm"
#include "includes/CryptoCommands/ROT.asm"
#include "includes/CryptoCommands/XOR.asm"
#include "includes/CryptoCommands/HELP.asm"

#include "includes/CryptoCommands/DECRYPT.asm"
#include "includes/CryptoCommands/unROT.asm"
#include "includes/CryptoCommands/unXOR.asm"
#include "includes/CryptoCommands/unHELP.asm"
