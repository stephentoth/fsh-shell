cryptocmdunROT:  .db "ROT", 0
cryptocmdunROTm: .db "ROT: Cypher, rotates the alphabet and Decodes a string", 0
cryptocmdunROTu: .db "DECRYPT ROT <shift#>", 0
cryptocmdunROTf:
cryptocmdunROTfArg1: 
 ld hl, FshCmdArgFlags
 bit 3, (hl)
 jr nz, cryptocmdunROTfArg2
 bit 2, (hl)
 jr z, cryptocmdunROTfArg1Get
 ld hl, ErrUsageo
 call ErrSet
 call ErrCatch
cryptocmdunROTfArg1Get:
 ld hl, cryptocmdunROTfDShift
 call printprompt
 ld hl, FshCmdArg1
 ld de, 3
 call inputnumstr
 call strtow
 ld de, FshCmdArg1
 ex de, hl
 ld (hl), e
 call _NewLine
cryptocmdunROTfArg2:
 ld hl, FshCmdArg2
 ld de, FshScratch
 call strcpy
 ld hl, FshCmdArgFlags
 bit 4, (hl)
 jr z, cryptocmdunROTfArg2Get
 bit 5, (hl)
 jr z, cryptocmdunROTfExe
 ld hl, ErrUsageo
 call ErrSet
 call ErrCatch
cryptocmdunROTfArg2Get:
 ld hl, cryptocmdunROTfDMessage
 call printprompt
 ld hl, FshScratch
 ld de, FshScratchSize - 4
 call inputcharstr
 call _NewLine
cryptocmdunROTfExe:
 ld hl, FshCmdArg1
 ld a, (hl)
 ld hl, FshScratch
 call rotletdecrypt
  push hl
  ld hl, cryptocmdunROTfDResult
  call _PutS
 pop hl
 call _PutS
 call _NewLine
 jp Fin

cryptocmdunROTfDShift: .db "Shift= ", 0
cryptocmdunROTfDMessage: .db "CypherText= ", 0
cryptocmdunROTfDResult: .db "Result: ", 0