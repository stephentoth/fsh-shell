;;Commanddecode
;;Decodes all commands.
;;Inputs:
;; HL: Cmd String mem loc
;; DE: Op code mem loc
;;Output:
;; Sets (DE) with command op code
commanddecode:
#macro decodecmdopt(cmdXs, cmdXo)
 push hl
  push de
   ld de, cmdXs
   call strcmp
   ld hl, cmdXo
  pop de
  call z, wordcpy
 pop hl
#endmacro

 call fshcmddecode
 call cryptocmddecode
 ;Extention Point

 ret

;;commandroute
;;Routes all commamds.
;;Inputs:
;; HL: Op code mem loc
commandroute:
#macro routeoptjp(cmdXo, cmdXf)
 ld de, cmdXo
 call wordcmp
 jp z, cmdXf
#endmacro

 call fshcmdroute
 call cryptocmdroute
 ;Extention Point

 ld hl, ErrCmdNotFoundo
 ld de, FshCmdErrCode
 call bytecpy
 call ErrSet
 jp Err

#include "includes/FshCommands/FshCommand.asm"
#include "includes/CryptoCommands/CryptoCommand.asm"
