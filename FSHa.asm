#include "includes/static/ti84pce.inc"
#include "includes/static/define.inc"
#include "includes/macros/macros.inc"

 .assume ADL=1
 .org userMem-2
 .db tExtTok,tAsm84CeCmp

Setup: 
 CALL _RunIndicOff
 CALL _EnableAPD
 SET apdRunning,(iy+apdFlags) 
 CALL _homeup
 CALL _ClrLCDFull

 CALL fshvarreset

 CALL drawstatusbar

 CALL printmotd

Input:
 LD   A, $00
 CALL commandfill

 LD   HL, PS1
 CALL printprompt 

 LD   HL, FshCmd
 LD   DE, FshCmdInptSize
 CALL inputstr

 LD   HL, FshCmd
 CALL fshparsecommand

 CALL _NewLine

Decode:
 LD   HL, FshCmd
 LD   DE, FshCmdOP
 CALL commanddecode

Route:
 LD   HL, FshCmdOP
 JP   commandroute

Fin:
 CALL updatestatusbar
 JP   Input

Err:
 JP   ErrRoute

Cleanup:
 SET  graphdraw,(IY+graphflags)
 CALL _ClrLCDFull
 CALL _ClrTxtShdw 
 CALL _homeup
 call _DrawStatusBar

Exit:
 RES  donePrgm,(iy+doneFlags)
 RET

#include "includes/Text.inc"
FshFlags: .DB %00000000
; 0: Debug Flag
; 1: Error Flag
; 2: Program Flag

#include "includes/lib/std/std.inc"
#include "includes/lib/fsh/fsh.inc"
#include "includes/lib/crypto/crypto.inc"

#include "includes/man-db.asm"
#include "includes/Command.asm"

.end
